default:
  tags:
    - puppet-server

stages:
  - cloneR10k

clonecode:
  stage: cloneR10k
  script:
    - r10k deploy environment -p
  only:
    - main